from peewee import SqliteDatabase, Model


class ImproperlyConfigured(Exception):
    pass


class Database(SqliteDatabase):

    def __init__(self):
        pass

    def init_app(self, app):
        self.app = app
        key = 'DATABASE'
        if self.app.debug:
            key = key + '_DEBUG'

        try:
            path = app.config[key]
        except KeyError:
            raise ImproperlyConfigured('Please set config %s' % key)

        super(Database, self).__init__(path)
        self.Model = self.get_model_class()
        self.register_handlers()

    def get_model_class(self):
        class CurrentModel(Model):
            class Meta:
                database = self

        return CurrentModel

    def safe_close(self, exc):
        if not self.is_closed():
            self.close()

    def register_handlers(self):
        self.app.before_request(self.connect)
        self.app.teardown_request(self.safe_close)


db = Database()
