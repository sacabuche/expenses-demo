from flask import abort, g

from flask.ext.basicauth import BasicAuth
from peewee import DoesNotExist


class DbBasicAuth(BasicAuth):

    def init_app(self, app):
        self.app = app
        super(DbBasicAuth, self).init_app(app)

    def challenge(self):
        abort(401)

    def check_credentials(self, username_or_token, password):
        User = self.app.User
        user = User.verify_auth_token(username_or_token)
        if not user:
            try:
                user = User.from_username(username_or_token)
            except DoesNotExist:
                return False
            if not user.check_password(password):
                return False
        g.user = user
        return True


auth = DbBasicAuth()
