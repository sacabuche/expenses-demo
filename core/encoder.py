from datetime import datetime

from flask.json import JSONEncoder


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime):
            # Always using Zulu date time
            return obj.isoformat() + 'Z'
        return JSONEncoder.default(self, obj)
