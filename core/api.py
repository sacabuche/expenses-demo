from flask import Blueprint, jsonify, g, render_template

from .auth import auth


api = Blueprint('api', __name__, url_prefix='/api/v1.0')
frontend = Blueprint('frontend', __name__, template_folder='templates')


@api.route('/token/')
@auth.required
def get_token():
    token = g.user.generate_auth_token()
    return jsonify({
        'id': g.user.id,
        'token': token.decode('ascii')
    })


@frontend.route('/')
def index():
    return render_template('core/index.html')
