from functools import partial

from flask import abort, jsonify
from peewee import SelectQuery


unauthorized = partial(abort, 401)


def sanitizer(obj, required=None, optional=None, force_one=False):
    if obj is None:
        abort(400)

    if not required:
        required = []
    if not optional:
        optional = []

    try:
        result = {field: obj[field] for field in required}
    except KeyError:
        abort(400)

    result.update({field: obj[field]
                   for field in optional if obj.get(field) is not None})

    if force_one and not len(result):
        abort(400)

    return result


def get_sanitizer(*args, **kwargs):
    return partial(sanitizer, *args, **kwargs)


def get_serializer(fields):
    def get_fields(obj):
        return {field: getattr(obj, field) for field in fields}

    def f(obj):
        if isinstance(obj, (list, tuple, SelectQuery)):
            result = [get_fields(o) for o in obj]
        else:
            result = [get_fields(obj)]
        return jsonify({
            'objects': result,
            'length': len(result),
        })
    return f


def verify_or_401(*args):
    """If all are false will raise unauthorized()"""

    for pair in args:
        # Falsy value
        if isinstance(pair, bool):
            if pair:
                return
            continue

        # only a function whiout arguments
        if callable(pair):
            if pair():
                return
            continue

        # call function and aboir if False
        f, f_args = pair
        if f(*f_args):
            return
    unauthorized()
