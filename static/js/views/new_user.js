define(['ko', 'config', 'tools/http', 'tools/base_view', 'page'], function(ko, config, http, BaseView, page) {
  'use strict';

  function View(app) {
    BaseView.call(this);
    var self = this;

    this.update_token = function() {};


    this.new_user = function() {
      if (!self.is_password_ok()) {
        alert('Verify your password and user');
        return;
      }

      http.user.post(self.username(), self.password()).done(function() {
        self.username('');
        self.password('');
        self.password_check('');
        alert('Nice, you are new!');
        page('/login');
      }).fail(function() {
        alert('Take other user please');
      });
    };

    self.username = ko.observable();
    self.password = ko.observable();
    self.password_check = ko.observable();
    self.is_password_ok = ko.computed(function() {
      var u = self.username(),
        p = self.password(),
        p2 = self.password_check();
      return u && p && (p === p2);
    });


    self.cancel = function() {
      page('/login');
    };
  }

  return View;

});
