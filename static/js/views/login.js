define(['ko', 'config', 'tools/http', 'tools/base_view', 'page'], function(ko, config, http, BaseView, page) {
  'use strict';

  function View(app) {

    BaseView.call(this);

    var self = this;
    this.username = ko.observable();
    this.password = ko.observable();
    this.error_message = ko.observable();

    this.update_token = function() {};

    this.before_hide = function() {
      self.error_message('');
    };

    this.login = function() {
      config.user.username = this.username();
      http.token.get(this.username(), this.password()).done(function(data) {
        self.password('');
        self.error_message('');
        console.log('logged in');
        //app.current_user(config.user);
      }).then(function() {
        return http.user.get(config.user.id);
      })
      .then(function (data) {
        config.user = data.objects[0];
        config.save();
        app.current_user(config.user);
        page('/home/first');
      })
      .fail(function() {
        self.error_message('user and password doesn\'t match');
      });
    };

    this.new_user = function() {
      page('/new_user');
    };
  }

  return View;

});
