define([
    'ko',
    'tools/http',
    'config',
    'tools/base_view',
    'moment',
    'page'
], function(ko, http, config, BaseView, moment, page) {

  function Day(date) {
    var self = this;
    self.number = date.isoWeekday();

    self.expenses = [];
    self.total = 0;
    self.add_expense = function (expense) {
      self.total += expense.amount;
      self.expenses.push(expense);
    };
  }

  function Week(date) {
    var self = this;
    self.number = date.isoWeek();
    self.title = date.format('W');
    self.year = date.year();
    self.days = [];
    self.total = 0;
    self.mean_x_day = 0;

    self.add_day = function(day) {
      self.days.push(day);
    };

    self.update_totals = function() {
      self.total = 0;
      for (var i in self.days) {
        self.total += self.days[i].total;
      }

      if (self.days.length === 0) {
        self.mean_x_day = 0;
      } else {
        self.mean_x_day = (self.total / 7).toFixed(2);
      }
    };
  }

  function json_to_date(date_txt) {
    // Adjust the utc offset
    return moment(date_txt);
  }

  function sort_by_week(expenses) {
    var weeks = [],
    week = {},
    day = {},
    expense,
    is_same_year, is_same_week;

    expenses.forEach(function (expense) {
      is_new_year = expense.date_time.year() !== week.year;
      is_new_week = expense.date_time.isoWeek() !== week.number;
      is_other_day = is_new_year || is_new_week || (expense.date_time.isoWeekday() !== day.number);

      if (is_new_year || is_new_week) {
        week = new Week(expense.date_time);
        weeks.push(week);
      }

      if (is_other_day) {
        day = new Day(expense.date_time);
        week.add_day(day);
      }

      day.add_expense(expense);
    });

    weeks.forEach(function (week) {
      week.update_totals();
    });

    return weeks;
  }


  function gen_filter(date, type) {
    return {
      start: date.clone().startOf(type),
      end: date.clone().endOf(type)
    };
  }


  function View(app) {

    BaseView.call(this);

    var self = this;

    self.is_super = ko.observable();
    self.filter = ko.observable('day');
    self.expenses = ko.observable([]);
    self.weeks = ko.observable([]);
    self.by_week = ko.observable();
    self.current_date = ko.observable();
    self.current_filter = ko.observable(null);
    self.current_user = ko.observable('');
    self.all_users = ko.observable([]);

    function set_default_settings() {
      self.is_super(config.user.is_super);
      self.by_week(false);
      self.current_user('');
      self.current_date(moment());
      self.filter('day');
    }

    set_default_settings();

    self.from_to_title = ko.computed(function () {
      var range = self.current_filter();
      if (!range) {
        return '';
      }

      if (range.start.isSame(range.end, 'day')) {
        return range.start.format('L');
      }

      return [range.start.format('L'), range.end.format('L')].join(' - ');
    });

    this.before_show = function(is_login) {

      console.log('firs', app.first_home);
      if (is_login || app.first_home) {
        app.first_home = false;
        set_default_settings();
        load_all_users();
        console.log('IS LOGIN');
      } 

      self.current_filter(gen_filter(self.current_date(), self.filter()));
      load_expenses(self.current_filter());
    };


    this.before_hide = function() {
      var current = app.current_user();
      if (!(current && current.id)) {
        self.all_users([]);
      }
    };

    function next_last(action) {
      var date = self.current_date();
      var filter = self.filter();

      if (filter === 'isoweek') {
        filter = 'week';
      }

      date[action](1, filter + 's');
      self.current_date(date);
    } 

    this.next = function() {
      next_last('add');
    };

    this.last = function() {
      next_last('subtract');
    };

    this.set_now = function() {
      self.current_date(moment());
    };

    this.new_edit_expense = function(expense) {
      // Dirty trick
      app.expense = expense;
      page('/home/expense');
    };

    this.edit_user = function() {
      page('/edit_user');
    };

    this.toggleByWeek = function () {
      self.by_week(!self.by_week());
    };

    function update(filter_name) {
      var user = app.current_user();
      if (!user || !user.id) {
        return;
      }
      self.current_filter(gen_filter(self.current_date(), self.filter()));
      load_expenses(self.current_filter());
    }

    var LOCK = false;
    function load_expenses(filter) {
      if (LOCK) {
        return;
      }
      LOCK = true;
      http.expenses.from_user(app.current_user().id, filter).done(function(data) {
        var expense;
        var tmp_date;
        for (var i in data.objects) {
          expense = data.objects[i];
          expense.date_time = json_to_date(expense.date_time);
        }
        self.expenses(data.objects);
        self.weeks(sort_by_week(data.objects));
      }).always(function() {
        LOCK = false;
      })
    }

    function load_all_users() {
      if (!config.user.is_super) {
        return;
      }

      return http.user.get_all().done(function (data) {
        self.all_users(data.objects);
        self.current_user(app.current_user());
      });

    };

    function update_current_user(user) {
      if (!user) {
        return;
      }

      var current = app.current_user();
      if (!(current && current.id)) {
        return;
      }

      if (current.id === user.id) {
        return;
      }

      console.log('Ya llego hasta aca', user);
      app.current_user(user);
      update();
    }


    self.filter.subscribe(update);
    self.current_date.subscribe(update);
    self.current_user.subscribe(update_current_user);
  }

  return View;
});
