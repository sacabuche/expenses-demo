define(['ko', 'tools/http', 'tools/base_view', 'jquery', 'moment', 'page'], function(ko, http, BaseView, $, moment, page) {
  'user strict';


  function fix_width(n) {
    if (n < 10) {
      return '0' + n;
    }
    return n;
  }


  function Expense(o, user) {
    var self = this;
    self.raw_o = o;
    self.user = user;

    self.map_object = function(o, names) {
      var name, value;
      self.o = {};
      for (var i in names) {
        name = names[i];
        value = ko.observable(o && o[name]);
        self.o[name] = value;
        self[name] = value;
      }
    };

    function parseTime(date) {
      var hours = date.hours(),
        minutes = date.minutes();
      return [fix_width(hours), fix_width(minutes)].join(':');
    }

    function date_to_str(d) {

      var year = d.year(),
        month = d.month() + 1,
        day = d.date();
      return [year, fix_width(month), fix_width(day)].join('-');
    }

    var d = o && o.date_time;
    self.raw_date = ko.observable(d && date_to_str(d));
    self.raw_time = ko.observable(d && parseTime(d));

    self.map_object(o, [
      'id',
      'description',
      'amount',
      'comment',
      'date_time'
    ]);

    self.raw_date.subscribe(function(d) {
      console.log(d);
      var date_time = self.date_time();

      if (!date_time) {
        date_time = moment();
      }

      var year, month, day;
      if (typeof d === 'string') {
        d = d.split('-');

        if (d.length == 3) {
          year = d[0];
          month = d[1] - 1;
          day = d[2];

        } else {
          var tmp = moment();
          year = tmp.getFullYear();
          month = tmp.getMonth();
          day = tmp.getDate();
        }

        console.log(year, month, day);
        date_time.year(year);
        date_time.month(month);
        date_time.date(day);
        self.date_time(date_time);
      }
    });


    self.raw_time.subscribe(function(t) {
      var date_time = self.date_time();
      console.log(date_time);

      if (!date_time) {
        date_time = moment();
      }

      if (typeof t === 'string') {
        t = t.split(':');
        if (t.length !== 2) {
          t = [0, 0];
        }
        date_time.hours(parseInt(t[0], 10));
        date_time.minutes(parseInt(t[1], 10));
        date_time.seconds(0);
      }

      self.date_time(date_time);
      console.log(t, date_time, date_time.toJSON());
    });

    // self.id = o.id;
    // self.date_time = ko.observable(o.date_time);
    // self.comment = ko.observable(o.comment);
    // self.description = ko.observable(o.description);
    // self.amount = ko.observable(o.amount);

    self.display_date_time = ko.computed(function() {
      var date_time = self.date_time();

      if (!date_time) {
        return '';
      }

      return moment(date_time).format('llll');
    });



    self.put = function() {
      console.log('put', ko.toJS(self.o));
      return http.expenses.put(self.user.id, self.o.id(), self.o);
    };

    self.post = function() {
      console.log('post', ko.toJS(self.o));
      return http.expenses.post(self.user.id, self.o);
    };

    self.del = function() {
      console.log('delete', ko.toJS(self.o));
      return http.expenses.del(self.user.id, self.o.id());
    };

  }


  function ExpenseView(app) {
    BaseView.call(this);

    var self = this;
    self.expense = ko.observable();

    self.is_edit = ko.observable();

    self.title = ko.computed(function() {
      if (self.is_edit()) {
        return 'Edit Expense';
      }
      return 'New Expense';
    });

    self.before_show = function() {
      var o;
      // This is for the dirty tick
      if (app.expense) {
        o = app.expense;
        delete app.expense;
      }
      console.log('Expense:', o);
      self.is_edit(o && o.id || false);
      self.expense(new Expense(o, app.current_user()));
    };


    self.cancel = function() {
      page('/home');
    };

    self.save = function() {
      var request;
      var expense = self.expense();
      if (self.is_edit()) {
        request = expense.put();
      } else {
        request = expense.post();
      }

      request.done(function() {
        page('/home');
      }).fail(function() {
        alert('Ups, You just destroyed the server!');
      });
    };

    self.del = function() {
      self.expense().del().done(function() {
        page('/home');
      }).fail(function() {
        alert('Nooooo!, you just released Skynet');
      });
    };


  }

  return ExpenseView;
});
