define(['ko', 'config', 'tools/http', 'tools/base_view', 'page'], function(ko, config, http, BaseView, page) {
  'use strict';

  function View(app) {
    BaseView.call(this);
    var self = this;

    self.current_user = app.current_user;
    self.is_super = ko.observable(false);

    this.before_show = function() {
      self.is_super(self.current_user().is_super);
    };

    this.save = function() {
      http.user.put(self.current_user().id, {
        is_super: self.is_super()
      }).done(function() {
        page('/home');
      }).fail(function() {
        alert('Do you remember the movie of the ring? Be ready (FAIL)');
      });
    };

    this.cancel = function() {
      page('/home');
    };

  }

  return View;

});
