define(['ko', 'tools/http', 'config', 'page'], function(ko, http, config, page) {
  'use strict';

  function BaseView() {

    var self = this;
    this.visible = ko.observable(false);

    this.before_show = function() {};
    this.after_show = function() {};
    this.before_hide = function() {};
    this.after_hide = function() {};

    this.update_token = function () {
      http.token.get().fail(function() {
        config.clean();
        page('/login');
      });
    };

    // Hooks are not ready for async

    this.show = function(o) {

      // Hook
      this.before_show(o);

      this.visible(true);

      // Hook
      this.after_show(o);

      this.update_token();
    };




    this.hide = function() {

      // Hook
      this.before_hide();

      this.visible(false);

      // Hook
      this.after_hide();
    };
  }




  return BaseView;
});
