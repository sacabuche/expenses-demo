define(['jquery', 'ko', 'config'], function($, ko, config) {

  function post(url, data) {
    console.log(url);
    return $.ajax({
      type: 'POST',
      url: url,
      data: ko.toJSON(data),
      dataType: 'json',
      contentType: 'application/json'
    });
  }

  function put(url, data) {
    return $.ajax({
      type: 'PUT',
      url: url,
      data: ko.toJSON(data),
      dataType: 'json',
      contentType: 'application/json'
    });
  }

  function del(url) {
    return $.ajax({
      type: 'DELETE',
      url: url,
      dataType: 'json',
      contentType: 'application/json'
    });
  }

  function getAuthHeader(username, password) {
    if (!password) {
      password = 'unused';
    }

    if (!username) {
      username = config.auth.token;
    }

    var auth = 'Basic ' + btoa(username + ':' + password);
    return {
      Authorization: auth
    };
  }

  function getToken(username, password) {
    $.ajaxSetup({
      headers: getAuthHeader(username, password)
    });
    return $.getJSON(config.api.token, function(data) {
      /* update values */
      config.user.id = data.id;
      config.auth.token = data.token;
      config.save();
      setAuthHeader();
    });
  }

  function get_user(user_id) {
    return $.getJSON(config.api.users + user_id + '/');
  }

  function edit_user(user_id, data) {
    return put(config.api.users + user_id + '/', data);
  }

  function get_all_users() {
    return $.getJSON(config.api.users);
  }

  function setAuthHeader() {
    if (config.auth.token) {
      $.ajaxSetup({
        headers: getAuthHeader(config.auth.token)
      });
    }
  }


  function get_user_expenses(user_id, range) {
    var data = {};

    if (range) {
      if (range.start) {
        data.date_time__gte = range.start.toJSON();
      }
      if (range.end) {
        data.date_time__lte = range.end.toJSON();
      }
    }

    var uri = config.api.expenses.replace('{id}', user_id);
    return $.getJSON(uri, data, function(data) {
      console.log(data);
    });
  }

  function new_user(username, password) {
    var o = {
      username: username,
      password: password
    };
    return post(config.api.users, o);
  }

  function new_expense(user_id, expense) {
    var uri = config.api.expenses.replace('{id}', user_id);
    return post(uri, expense);
  }

  function edit_expense(user_id, expense_id, expense) {
    var uri = config.api.expenses.replace('{id}', user_id) + expense_id + '/';
    return put(uri, expense);
  }

  function delete_expense(user_id, expense_id) {
    var uri = config.api.expenses.replace('{id}', user_id) + expense_id + '/';
    return del(uri);
  }

  setAuthHeader();

  return {
    user: {
      post: new_user,
      get: get_user,
      get_all: get_all_users,
      put: edit_user
    },
    token: {
      get: getToken
    },
    expenses: {
      from_user: get_user_expenses,
      get: null,
      post: new_expense,
      put: edit_expense,
      del: delete_expense
    }
  };
});
