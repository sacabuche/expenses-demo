define(['lockr'], function (lockr) {
  var BASE_API = '/api/v1.0';

  var C = {};


  C.api = {
    token: BASE_API + '/token/',
    expenses: BASE_API + '/users/{id}/expenses/',
    users: BASE_API + '/users/'
  };

  C.save = function () {
    lockr.set('user', C.user);
    lockr.set('auth', C.auth);
  };

  C.clean = function () {
    lockr.flush();
    set_C();
  };


  function set_C() {
    C.user = lockr.get('user', {});
    C.auth = lockr.get('auth', {});
  }

  set_C();

  return C;

});
