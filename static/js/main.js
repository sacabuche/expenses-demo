requirejs.config({

  paths: {
    'domReady': 'libs/domReady',
    'ko': 'libs/knockout-3.3.0',
    'jquery': 'libs/jquery-2.1.4.min',
    'lockr': 'libs/lockr.min',
    'moment': 'libs/moment.min',
    'page': 'libs/page'
  }
});


define(['ko', 'config', 'page', 'jquery', 'domReady!'], function(ko, config, page, $) {
  'use strict';

  var LOGIN = 'login';
  var HOME = 'home';
  var NEW_USER = 'new_user';

  var LOGED = [HOME, 'add_edit_expense', 'edit_user'];
  var LOGOUT = [LOGIN, NEW_USER];

  var current_view_name = ko.observable(); 


  var VIEWS = {
  };

  function App() {

    var self = this;

    // need to init this.view using nav_to
    // self.view = loginView

    this.first_home = true;
    self.current_user = ko.observable(config.user);
    self.nav_to = function nav_to(name, o) {

      current_view_name(name);
      if (self.view) {
        self.view.hide();
      }

      var current = VIEWS[name];
      if (current) {
        self.view = current;
        current.show(o);
        return;
      }

      require(['views/' + name], function (View) {
        self.view = new View(self);
        self.view.app = self;
        self.view.show(o);
        ko.applyBindings(self.view, document.getElementById(name));
        VIEWS[name] = self.view;
      });
    };

  }


  function HeaderViewModel(app) {
    var self = this;
    self.current_user = app.current_user;
    self.showLogOut = ko.computed(function() {
      var current = current_view_name();
      for (var i in LOGED) {
        if (LOGED[i] == current) {
          return true;
        }
      }
      return false;
    });

    self.logOut = function() {
      config.clean();
      app.current_user({});
      page('/login');
    };
  }

  var app = new App();

  page('/', function () {
    if (config.user.id) {
      page('/home/first');
    } else {
      page('/login');
    }
  });

  page('/login', function() {
    app.nav_to(LOGIN);
  });

  page('/home/first', function() {
    app.nav_to(HOME, true);
  });

  page('/home', function() {
    app.nav_to(HOME);
  });

  page('/new_user', function() {
    app.nav_to('new_user');
  });

  page('/edit_user', function() {
    app.nav_to('edit_user');
  });

  page('/home/expense', function () {
    app.nav_to('add_edit_expense');
  });

  page({hashbang: true});

  /* Force manual listening */
  $(window).on('hashchange', function() {
    var hash = location.hash.slice(1);
    hash = hash.replace('!', '');
    page(hash);
  });

  // ko.applyBindings(VIEWS.home, document.getElementById('home'));
  ko.applyBindings(new HeaderViewModel(app), document.getElementById('header'));
});
