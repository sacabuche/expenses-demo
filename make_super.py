import sys
from entry import app
from users.models import User


def make_super(user_name):
    try:
        user = User.from_username(user_name)
    except:
        print('user Doesn\'t exists')
        return
    if (user.is_super):
        print ('%s is already super!' % user_name)
        return
    user.is_super = True
    user.save()
    print('%s now is super!' % user_name)


if __name__ == '__main__':
    make_super(sys.argv[1])
