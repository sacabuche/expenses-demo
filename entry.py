from app import create_app

app = create_app(config='local.cfg', app_name='all_expenses')


if __name__ == '__main__':
    app.run()
