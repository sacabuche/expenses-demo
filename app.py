import os
from flask import Flask, g
from core import db, auth, CustomJSONEncoder


BASE_CONFIG = 'core.config'


def create_app(config=None, app_name='project'):
    static_folder = os.path.join(os.path.dirname('__file__'), 'static')
    app = Flask(app_name,
                static_folder=static_folder,
                template_folder='templates')

    app.json_encoder = CustomJSONEncoder
    # Base local config
    app.config.from_object(BASE_CONFIG)

    # Local config
    # app.config.from_pyfile('local.cfg', silent=True)
    if config:
        app.config.from_pyfile(config)

    extensions_fabrics(app)
    blueprints_fabrics(app)

    gvars(app)
    return app


def blueprints_fabrics(app):
    from core.api import frontend, api as tokens
    from users.api import api as users
    from expenses.api import api as expenses

    blueprints = (
        tokens,
        users,
        expenses,
        frontend,
    )

    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def extensions_fabrics(app):
    db.init_app(app)

    from users import User
    from expenses.models import Expense
    db.create_table(User, safe=True)
    db.create_table(Expense, safe=True)

    # Init auth
    app.User = User
    auth.init_app(app)


def gvars(app):
    @app.before_request
    def gdebug():
        if app.debug:
            g.debug = True
        else:
            g.debug = False
