from datetime import datetime

from peewee import TextField, ForeignKeyField, DateTimeField, FloatField

from core import db
from users import User


app = db.app


class Expense(db.Model):
    user = ForeignKeyField(User, related_name='expenses')
    date_time = DateTimeField(default=datetime.utcnow)
    amount = FloatField()
    description = TextField()
    comment = TextField(default='')

    @classmethod
    def get_it(cls, user_id, id):
        is_user = cls.user == user_id
        is_expense = cls.id == id
        return cls.get(is_user & is_expense)
