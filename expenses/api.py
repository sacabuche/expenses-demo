from datetime import datetime

from flask import Blueprint, g, request, abort
from peewee import DoesNotExist

from core import auth
from core.tools import verify_or_401, get_serializer, get_sanitizer
from users.models import User

from .models import Expense


api = Blueprint('expenses', __name__,
                url_prefix='/api/v1.0/users/<int:user_id>/expenses')


serialize = get_serializer(fields=['id', 'date_time', 'amount',
                                   'description', 'comment'])


new_sanitizer = get_sanitizer(required=['amount', 'description'],
                              optional=['date_time', 'comment'])

edit_sanitizer = get_sanitizer(optional=['amount', 'description',
                                         'date_time', 'comment'],
                               force_one=True)


JSON_DATETIME_STRING = '%Y-%m-%dT%H:%M:%S.%fZ'



def json_to_date(d):
    try:
        return datetime.strptime(d, JSON_DATETIME_STRING)
    except ValueError:
        abort(401)


def perms_or_401(user_id):
    user = g.user
    verify_or_401(user.is_super,
                  user.id == user_id)


def user_or_404(user_id):
    try:
        return User.get(User.id == user_id)
    except DoesNotExist:
        abort(404)


def user_expense_or_404(user_id, expense_id):
    try:
        return Expense.get(Expense.user == user_id,
                           Expense.id == expense_id)
    except DoesNotExist:
        abort(404)


@api.route('/')
@auth.required
def list_user_expenses(user_id):
    perms_or_401(user_id)
    user = user_or_404(user_id)

    gte__date_time = request.args.get('date_time__gte', None)
    lte__date_time = request.args.get('date_time__lte', None)

    query = Expense.select().where(Expense.user == user)
    if gte__date_time:
        print(gte__date_time)
        query = query.where(Expense.date_time >= json_to_date(gte__date_time))
    if lte__date_time:
        query = query.where(Expense.date_time <= json_to_date(lte__date_time))

    query = query.order_by(Expense.date_time.desc())
    return serialize(query)


@api.route('/', methods=['POST'])
@auth.required
def create_user_expense(user_id):
    perms_or_401(user_id)
    values = new_sanitizer(request.json)

    user = user_or_404(user_id)
    try:
        date = values['date_time']
    except KeyError:
        pass
    else:
        values['date_time'] = json_to_date(date)

    expense = Expense.create(user=user, **values)
    return serialize(expense), 201


@api.route('/<int:expense_id>/', methods=['GET'])
@auth.required
def get_user_expense(user_id, expense_id):
    perms_or_401(user_id)
    expense = user_expense_or_404(user_id, expense_id)
    return serialize(expense)


@api.route('/<int:expense_id>/', methods=['PUT'])
@auth.required
def edit_user_expense(user_id, expense_id):
    perms_or_401(user_id)
    values = edit_sanitizer(request.json)
    expense = user_expense_or_404(user_id, expense_id)

    try:
        date = values['date_time']
    except KeyError:
        pass
    else:
        values['date_time'] = json_to_date(date)

    update = Expense.update(**values).where(Expense.id == expense.id)
    assert update.execute()
    # expense = Expense.get(Expense.id == expense_id)
    return '', 204


@api.route('/<int:expense_id>/', methods=['DELETE'])
@auth.required
def delete_user_expense(user_id, expense_id):
    perms_or_401(user_id)
    try:
        expense = Expense.get(Expense.id == expense_id,
                              Expense.user == user_id)
    except DoesNotExist:
        return '', 204

    expense.delete_instance()
    return '', 204
