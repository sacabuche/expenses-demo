from enum import Enum, unique

from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from peewee import CharField, IntegerField, DoesNotExist, BooleanField

from core import db

app = db.app


EXPIRE_TIME = app.config['TOKEN_EXPIRE_MINUTES'] * 60


@unique
class Role(Enum):
    user = 1
    manager = 2


class User(db.Model):
    username = CharField(max_length=100, unique=True)
    password_hash = CharField(max_length=128)
    is_super = BooleanField(default=False)
    role = IntegerField(default=Role.user.value)

    def is_role(self, enum):
        return enum.value == self.role

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration=EXPIRE_TIME):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @classmethod
    def verify_auth_token(cls, token):
        """Returns some user if is a valid signature"""
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # expired
        except BadSignature:
            return None  # invalid

        try:
            return cls.get(cls.id == data['id'])
        except DoesNotExist:
            return None

    @classmethod
    def create(cls, username, password, **kwargs):
        user = cls(username=username, **kwargs)
        user.set_password(password)
        user.save()
        return user

    @classmethod
    def from_username(cls, username):
        return cls.get(cls.username == username)
