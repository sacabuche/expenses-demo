from flask import Blueprint, request, abort, g
from peewee import DoesNotExist

from core import auth
from core.tools import get_sanitizer, get_serializer, verify_or_401
from users import User
from users.models import Role

new_sanitizer = get_sanitizer(required=['username', 'password'],
                              optional=['role'])
edit_sanitizer = get_sanitizer(optional=['role', 'is_super', 'password'])
serializer = get_serializer(fields=['id', 'username', 'is_super', 'role'])


api = Blueprint('users', __name__, url_prefix='/api/v1.0/users')


def perms_or_401(user_id):
    verify_or_401(g.user.id == user_id,
                  g.user.is_super,
                  g.user.is_role(Role.manager))


@api.route('/', methods=['POST'])
def new_user():
    values = new_sanitizer(request.json)
    try:
        User.from_username(values['username'])
    except DoesNotExist:
        pass
    else:
        abort(400)

    user = User.create(**values)
    # url = url_for('.get_user', id=user.id, _external=True)
    return serializer(user), 201


@api.route('/<int:id>/', methods=['GET'])
@auth.required
def get_user(id):
    perms_or_401(id)

    try:
        user = User.get(User.id == id)
    except DoesNotExist:
        abort(404)

    return serializer(user)


@api.route('/<int:id>/', methods=['PUT'])
@auth.required
def edit_user(id):
    perms_or_401(id)

    values = edit_sanitizer(request.json)
    try:
        user = User.get(User.id == id)
    except DoesNotExist:
        abort(404)

    changed = False
    password = values.pop('password',  None)
    role = values.pop('role', None)
    is_super = values.pop('is_super', None)

    if password is not None:
        changed = True
        user.set_password(password)

    if role and (role > 0) and (role <= 2):
        changed = True
        user.role = role

    if (is_super is not None) and g.user.is_super:
        changed = True
        user.is_super = is_super

    if not changed:
        #  Bad request
        return '', 400

    user.save()
    return '', 204


@api.route('/<int:id>/', methods=['DELETE'])
@auth.required
def delete_user(id):
    perms_or_401(id)

    try:
        user = User.get(User.id == id)
    except DoesNotExist:
        return '', 204

    user.delete_instance(recursive=True)
    return '', 204


@api.route('/')
@auth.required
def list_users():
    verify_or_401(g.user.is_super,
                  g.user.is_role(Role.manager))
    query = User.select(User.username, User.id, User.is_super, User.role)
    return serializer(query)
